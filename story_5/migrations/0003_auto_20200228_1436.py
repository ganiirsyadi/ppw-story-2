# Generated by Django 3.0.3 on 2020-02-28 07:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('story_5', '0002_auto_20200228_1414'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='assignment',
            options={'ordering': ['date']},
        ),
    ]
